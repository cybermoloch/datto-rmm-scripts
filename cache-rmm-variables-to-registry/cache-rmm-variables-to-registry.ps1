<#PSScriptInfo

.VERSION 1.0.2

.AUTHOR cybermoloch@magitekai.com

.COPYRIGHT 2020 CyberMoloch. All Rights Reserved.

.LICENSEURI https://gitlab.com/cybermoloch/datto-rmm-scripts/-/raw/master/LICENSE

.PROJECTURI https://www.magitekai.com/#/blog/datto-rmm/2020/workaround-missing-variables

.RELEASENOTES
    Added metadata

#>

<#
.SYNOPSIS
    Caches Datto RMM Variables into the Registry

.DESCRIPTION
    This script when used with the included code snippet at the bottom allows for the usage of RMM Variables
    in User Tasks which are normally left out. This script should be ran daily on end-points which allow
    User Tasks.

.NOTES
    Version: 1.0.1
    Author: CyberMoloch <cybermoloch@magitekai.com>
    Creation Date: 2020-07-01
    Purpose/Change: Initial script development
    License: MIT

.LINK
    https://www.magitekai.com/#/blog/datto-rmm/2020/workaround-missing-variables

.LINK
    https://gitlab.com/cybermoloch/datto-rmm-scripts/-/tree/master/cache-rmm-variables-to-registry
#>
 

$envRegPath = 'HKLM:\SOFTWARE\CentraStage\Env'

# Remove and recreation makes it always clean
Remove-Item -Path $envRegPath -Force
New-Item -Path $envRegPath -Force

$envRegAcl = Get-Acl $envRegPath
# Disable Inheritance before changing ACLs
$envRegAcl.SetAccessRuleProtection($True, $True)
Set-Acl -Path $envRegPath -AclObject $envRegAcl

$envRegAcl = Get-Acl $envRegPath
# Remove all old ACLs
$envRegAcl.Access | ForEach-Object -Process {
    # Somtimes error below happens and ErrorAction SilentlyContinue doesn't work
    # Exception calling "RemoveAccessRule" with "1" argument(s): "Some or all identity references could not be translated."
    $envRegAcl.RemoveAccessRule($PSItem) | Out-Null
    }
# Set new ACLS for SYSTEM and Administrators
$regAclIds = @('NT AUTHORITY\SYSTEM','BUILTIN\Administrators')
$regAclIds | ForEach-Object -Process {
    $currentAcl = New-Object System.Security.AccessControl.RegistryAccessRule($PSItem,'FullControl','Allow')
    $envRegAcl.SetAccessRule($currentAcl)
}
Set-Acl -Path $envRegPath -AclObject $envRegAcl

# Export Env:\ to registry
# Excluding Windows defaults and others
# Exclude vs include list so the component does not need to be updated when new site/account variables are added

# Windows default Environment Variables that should exist even in user-initiated task
$winEnv = @(
    'ALLUSERSPROFILE',
    'APPDATA',
    'CommonProgramFiles',
    'CommonProgramFiles(x86)',
    'CommonProgramW6432',
    'COMPUTERNAME',
    'ComSpec',
    'DriverData',
    'LOCALAPPDATA',
    'NUMBER_OF_PROCESSORS',
    'OS',
    'Path',
    'PATHEXT',
    'PROCESSOR_ARCHITECTURE',
    'PROCESSOR_IDENTIFIER',
    'PROCESSOR_LEVEL',
    'PROCESSOR_REVISION',
    'ProgramData',
    'ProgramFiles',
    'ProgramFiles(x86)',
    'ProgramW6432',
    'PSExecutionPolicyPreference',
    'PSModulePath',
    'PUBLIC',
    'SystemDrive',
    'SystemRoot',
    'TEMP',
    'TMP',
    'USERDOMAIN',
    'USERNAME',
    'USERPROFILE',
    'windir'
)
# Datto RMM (CentraStage) Agent Variables; maybe we want these?
$csEnv = @(
    'CS_ACCOUNT_UID',
    'CS_CC_HOST',
    'CS_CC_PORT1',
    'CS_CSM_ADDRESS',
    'CS_DOMAIN',
    'CS_PROFILE_DESC',
    'CS_PROFILE_NAME',
    'CS_PROFILE_PROXY_TYPE',
    'CS_PROFILE_UID',
    'CS_WS_ADDRESS'
)
# Other variables we know are not for our component (some from Microsoft/some user level[cleaner testing])
$otherEnv = @(
    'CS_ONLINETASK'
    'GTK_BASEPATH',
    'HOMEDRIVE',
    'HOMEPATH',
    'LOGONSERVER',
    'OneDrive',
    'USERDOMAIN_ROAMINGPROFILE',
    'VBOX_MSI_INSTALL_PATH',
    'WSLENV',
    'WT_PROFILE_ID',
    'WT_SESSION'
)

$excludeEnv = @()
$excludeEnv = ($winEnv + $csEnv + $otherEnv)
$envExport = (Get-ChildItem Env:\ | Where-Object -FilterScript { -not ($PSItem.Name -in $excludeEnv) } )
$envExport | ForEach-Object -Process { Set-ItemProperty -Path $envRegPath -Name $PSItem.Name -Value $PSItem.Value }

<# Snippet to insert into top of component scripts to pull RMM variables cached in registry

If (-not (Test-Path -Path Env:\CS_ONLINETASK) ) {
    $envRegPath = 'HKLM:\SOFTWARE\CentraStage\Env'
    # Exclude Environment Variables that are pulled when using PSDrive to read Registry
    $excludeEnv = @('PSChildName','PSDrive','PSParentPath','PSPath','PSProvider')
    (Get-ItemProperty $envRegPath).PSObject.Properties |
        Where-Object -FilterScript { -not ($PSItem.Name -in $excludeEnv) } |
        ForEach-Object -Process { Set-Item -Path ('Env:\' + $PSItem.Name) -Value $PSItem.Value }
}

#>